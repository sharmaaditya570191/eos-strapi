'use strict';

/**
 * Designspecs.js controller
 *
 * @description: A set of functions called "actions" for managing `Designspecs`.
 */

module.exports = {

  /**
   * Retrieve designspecs records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.designspecs.search(ctx.query);
    } else {
      return strapi.services.designspecs.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a designspecs record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.designspecs.fetch(ctx.params);
  },

  /**
   * Count designspecs records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.designspecs.count(ctx.query);
  },

  /**
   * Create a/an designspecs record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.designspecs.add(ctx.request.body);
  },

  /**
   * Update a/an designspecs record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.designspecs.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an designspecs record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.designspecs.remove(ctx.params);
  }
};
